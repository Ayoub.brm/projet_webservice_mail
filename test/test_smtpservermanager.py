import unittest
from smtpserver.smtpservermanager import SMTPServerManager
from config import SERVER_SMTP_HOST, SERVER_SMTP_PORT

class TestSMTPServerManager(unittest.TestCase):
    def setUp(self):
        self.smtp_server_manager = SMTPServerManager()

    def test_get_host(self):
        self.assertEqual(self.smtp_server_manager.get_host(), SERVER_SMTP_HOST)

    def test_get_port(self):
        self.assertEqual(self.smtp_server_manager.get_port(), SERVER_SMTP_PORT)

    def test_start(self):
        smtp_server = self.smtp_server_manager.start()
        self.assertIsNotNone(smtp_server)
        # Add any assertions you want to check here
        smtp_server.close()

    def test_stop(self):
        self.smtp_server_manager.stop()
        # Add any assertions you want to check here

if __name__ == '__main__':
    unittest.main()