import unittest
import sqlite3
from dao_mail.dbconnection import DBConnection

DB_NAME = "my_db.sqlite"  # modify with the name of your database


class TestDBConnection(unittest.TestCase):
    
    def setUp(self):
        self.connection = sqlite3.connect(DB_NAME)
        self.db_connection = DBConnection()
        
    def test_connection(self):
        self.assertEqual(self.connection, self.db_connection.connection())

    def tearDown(self):
        self.connection.close()
        
if __name__ == '__main__':
    unittest.main()