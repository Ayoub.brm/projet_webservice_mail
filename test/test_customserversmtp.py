import unittest
from smtpserver.smtp_server import CustomSMTPServer

class TestCustomSMTPServer(unittest.TestCase):
    def setUp(self):
        self.custom_smtp_server = CustomSMTPServer(('localhost', 25), None)

    def test_process_message(self):
        peer = ('127.0.0.1', 5000)
        mailfrom = 'sender@example.com'
        rcpttos = ['recipient1@example.com', 'recipient2@example.com']
        data = b'This is a test message.'
        self.custom_smtp_server.process_message(peer, mailfrom, rcpttos, data)

if __name__ == '__main__':
    unittest.main()