import unittest
from datetime import datetime
from dao_mail.maildao import MailDAO
from dao_mail.mail_request import MailRequest
from dao_mail.dbconnection import DBConnection



class TestMailDAO(unittest.TestCase):

    def setUp(self):
        self.dao = MailDAO()
        self.mail1 = MailRequest("sender1", "recipient1", "subject1", "body1", datetime.now(), "NON TRAITE")
        self.mail2 = MailRequest("sender2", "recipient2", "subject2", "body2", datetime.now(), "NON TRAITE")
        self.id_mail1 = self.dao.insert(self.mail1)
        self.id_mail2 = self.dao.insert(self.mail2)

    def tearDown(self):
        conn = DBConnection().connection()
        cursor = conn.cursor()
        cursor.execute("DELETE FROM table_demande")
        cursor.execute("DELETE FROM table_mail")
        conn.commit()

    def test_insert(self):
        mail3 = MailRequest("sender3", "recipient3", "subject3", "body3", datetime.now(), "NON TRAITE")
        id_mail3 = self.dao.insert(mail3)
        self.assertEqual(id_mail3, self.id_mail2 + 1)

    def test_find_by_id(self):
        mail = self.dao.find_by_id(self.id_mail1)
        self.assertEqual(mail.get_sender(), self.mail1.get_sender())
        self.assertEqual(mail.get_recipient(), self.mail1.get_recipient())
        self.assertEqual(mail.get_subject(), self.mail1.get_subject())
        self.assertEqual(mail.get_body(), self.mail1.get_body())
        self.assertEqual(mail.get_send_at(), self.mail1.get_send_at())
        self.assertEqual(mail.get_statut(), self.mail1.get_statut())

    def test_past_query(self):
        past_mail_id = self.dao.past_query(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.assertEqual(len(past_mail_id), 2)

    def test_maj(self):
        list_id = [self.id_mail1, self.id_mail2]
        self.dao.maj(list_id)
        mail1 = self.dao.find_by_id(self.id_mail1)
        mail2 = self.dao.find_by_id(self.id_mail2)
        self.assertEqual(mail1.get_statut(), "TRAITE")
        self.assertEqual(mail2.get_statut(), "TRAITE")

    def test_findAll(self):
        mails = self.dao.findAll()
        self.assertEqual(len(mails), 2)
        self.assertEqual(mails[0].get_sender(), self.mail1.get_sender())
        self.assertEqual(mails[0].get_recipient(), self.mail1.get_recipient())
        self.assertEqual(mails[0].get_subject(), self.mail1.get_subject())
        self.assertEqual(mails[0].get_body(), self.mail1.get_body())
        self.assertEqual(mails[0].get_send_at(), self.mail1.get_send_at())
        self.assertEqual(mails[0].get_statut(), self.mail1.get_statut())
        self.assertEqual(mails[1].get_sender(), self.mail2.get_sender())
        self.assertEqual(mails[1].get_recipient(), self.mail2.get_recipient())
        self.assertEqual(mails[1].get_subject(), self.mail2.get_subject())
        self.assertEqual(mails[1].get_body(), self.mail2.get_body())
        self.assertEqual(mails[1].get_send_at(), self.mail2.get_send_at())
        self.assertEqual(mails[1].get_statut(), self.mail2.get_statut())

if __name__ == '__main__':
    unittest.main()