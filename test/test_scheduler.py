import unittest
from unittest.mock import MagicMock
from datetime import datetime
from service.scheduler import Scheduler


class TestScheduler(unittest.TestCase):
    def test_start(self):
        # Test the start method with a mocked MailService object
        mail_service_mock = MagicMock()
        scheduler = Scheduler(intervall=5)
        scheduler.start(host='smtp.example.com', port=587)
        
        # Ensure that scheduledMail method is called with correct parameters
        mail_service_mock.scheduledMail.assert_called_with(datetime.now(), 'smtp.example.com', 587)
        
        # Ensure that time.sleep is called with the correct interval
        time_mock = MagicMock()
        with unittest.mock.patch('scheduler.time.sleep', time_mock):
            scheduler.start(host='smtp.example.com', port=587)
            time_mock.assert_called_with(5)

if __name__ == '__main__':
    unittest.main()