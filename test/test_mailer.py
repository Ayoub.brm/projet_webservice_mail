import unittest
from unittest.mock import MagicMock, patch
from service.mailer import Mailer
from dao_mail.mail_request import MailRequest

class TestMailer(unittest.TestCase):

    def test_send_calls_SMTP_with_correct_arguments(self):
        # arrange
        host = "smtp.gmail.com"
        port = 587
        mailer = Mailer()
        conn = MagicMock()
        mailer._create_connection = MagicMock(return_value=conn)
        request = MailRequest("sender@example.com", "recipient@example.com", "Test subject", "Test body")
        expected_msg = "Test subject\nTest body"

        # act
        mailer.send(request, host, port)

        # assert
        mailer._create_connection.assert_called_once_with(host, port)
        conn.sendmail.assert_called_once_with(request.get_sender(), [request.get_recipient()], expected_msg.encode('utf-8'))
        conn.quit.assert_called_once()

    def test_create_connection_calls_SMTP_with_correct_arguments(self):
        # arrange
        host = "smtp.gmail.com"
        port = 587
        mailer = Mailer()

        # act
        with patch('smtplib.SMTP') as mock_smtp:
            mailer._create_connection(host, port)

        # assert
        mock_smtp.assert_called_once_with(host, port)

if __name__ == '__main__':
    unittest.main()