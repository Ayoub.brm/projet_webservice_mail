import unittest
from unittest.mock import MagicMock
from datetime import datetime
from service.mailservice import MailService

class TestMailService(unittest.TestCase):

    def test_scheduledMail(self):
        # On crée une liste de mails à envoyer
        mails_to_send = [{'id': 1, 'from': 'test@test.com', 'to': 'test2@test.com', 'subject': 'Test', 'content': 'Contenu', 'send_date': datetime(2023, 3, 28)},
                         {'id': 2, 'from': 'test@test.com', 'to': 'test2@test.com', 'subject': 'Test', 'content': 'Contenu', 'send_date': datetime(2023, 3, 29)}]

        # On crée un objet MailDAO avec un mock qui retourne les mails à envoyer
        mailDAO = MagicMock()
        mailDAO.past_query.return_value = [1, 2]
        mailDAO.find_by_id.side_effect = lambda id: mails_to_send[id-1]

        # On crée un objet Mailer avec un mock pour vérifier l'appel de la méthode send
        mailer = MagicMock()

        # On crée un objet MailService avec les mocks créés
        mailService = MailService()
        mailService.Mailer = mailer
        mailService.MailDAO = mailDAO

        # On appelle la méthode scheduledMail avec une date et un host et port SMTP
        mailService.scheduledMail(datetime(2023, 3, 27), 'smtp.test.com', 587)

        # On vérifie que les méthodes de MailDAO ont bien été appelées avec les bons arguments
        mailDAO.past_query.assert_called_once_with(datetime(2023, 3, 27))
        mailDAO.find_by_id.assert_any_call(1)
        mailDAO.find_by_id.assert_any_call(2)
        self.assertEqual(mailDAO.find_by_id.call_count, 2)

        # On vérifie que la méthode send de Mailer a été appelée avec les bons arguments pour chaque mail à envoyer
        mailer.send.assert_any_call(mails_to_send[0], 'smtp.test.com', 587)
        mailer.send.assert_any_call(mails_to_send[1], 'smtp.test.com', 587)
        self.assertEqual(mailer.send.call_count, 2)

        # On vérifie que la méthode maj de MailDAO a bien été appelée avec les bons arguments
        mailDAO.maj.assert_called_once_with([1, 2])

if __name__ == '__main__':
    unittest.main()