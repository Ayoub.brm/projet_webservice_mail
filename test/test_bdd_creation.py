import unittest
import sqlite3
from unittest.mock import patch
from dao_mail.bdd_creation import Creation_bdd

class TestCreationBdd(unittest.TestCase):
    def setUp(self):
        self.creer_bdd = Creation_bdd()

    @patch("builtins.open")
    def test_creation_bdd_ouverture_fichier(self, mock_open):
        # On simule l'ouverture du fichier
        mock_open.return_value.__enter__.return_value.readlines.return_value = ["instruction1\n", "instruction2\n"]
        self.creer_bdd.creation_bdd()
        mock_open.assert_called_once_with("db_init.sql", "r")

    @patch.object(sqlite3, "connect")
    def test_creation_bdd_etablissement_connexion(self, mock_connect):
        mock_connexion = mock_connect.return_value
        self.creer_bdd.creation_bdd()
        mock_connect.assert_called_once_with("nom_de_la_bdd.db")
        mock_connexion.cursor.assert_called_once()

    @patch.object(sqlite3, "connect")
    def test_creation_bdd_execution_instructions_sql(self, mock_connect):
        mock_connexion = mock_connect.return_value
        mock_cursor = mock_connexion.cursor.return_value
        mock_cursor.fetchall.return_value = []
        self.creer_bdd.creation_bdd()
        expected_calls = [unittest.mock.call("instruction1\n"), unittest.mock.call("instruction2\n")]
        mock_cursor.execute.assert_has_calls(expected_calls)
        mock_connexion.commit.assert_called_once()

if __name__ == '__main__':
    unittest.main()