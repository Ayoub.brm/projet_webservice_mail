import unittest
import datetime
from dao_mail.mail_request import MailRequest

class TestMailRequest(unittest.TestCase):

    def setUp(self):
        self.sender = "john.doe@example.com"
        self.recipient = "jane.doe@example.com"
        self.subject = "Test Subject"
        self.body = "Test Body"
        self.send_at = datetime.datetime.now()

    def test_init(self):
        mail_request = MailRequest(self.sender, self.recipient, self.subject, self.body, self.send_at)
        self.assertEqual(mail_request.get_sender(), self.sender)
        self.assertEqual(mail_request.get_recipient(), self.recipient)
        self.assertEqual(mail_request.get_subject(), self.subject)
        self.assertEqual(mail_request.get_body(), self.body)
        self.assertEqual(mail_request.get_send_at(), self.send_at)
        self.assertEqual(mail_request.get_statut(), "NON TRAITE")

    def test_to_json(self):
        mail_request = MailRequest(self.sender, self.recipient, self.subject, self.body, self.send_at)
        request_json = mail_request.to_json()
        self.assertEqual(request_json["from"], self.sender)
        self.assertEqual(request_json["destinataire"], self.recipient)
        self.assertEqual(request_json["Subject"], self.subject)
        self.assertEqual(request_json["contenu"], self.body)
        self.assertEqual(request_json["date-envoi"], self.send_at.strftime("%a %b %d %H:%M:%S %Y"))
        self.assertEqual(request_json["etat en cours"], "NON TRAITE")

    def test_send_at_string_format(self):
        send_at_string = "Wed Oct 31 14:56:24 2023"
        mail_request = MailRequest(self.sender, self.recipient, self.subject, self.body, send_at_string)
        self.assertEqual(mail_request.get_send_at(), datetime.datetime.strptime(send_at_string, "%a %b %d %H:%M:%S %Y"))

if __name__ == '__main__':
    unittest.main()