import sqlite3
from config import DB_NAME

class DBConnection():  
    """
    Technical class to open only one connection to the DB.
    """
    def __init__(self):
        self.__connection = sqlite3.connect(DB_NAME)

    def connection(self):
        """
        return the opened connection.

        :return: the opened connection.
        """
        return self.__connection
