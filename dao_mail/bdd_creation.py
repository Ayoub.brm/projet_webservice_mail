from dao_mail.dbconnection import DBConnection

class Creation_bdd:
    """
    Classe qui permet la création de la base de données en exécutant des instructions SQL stockées dans un fichier.
    """

    def creation_bdd(self):
        """
        Méthode qui lit les instructions SQL du fichier 'db_init.sql', établit une connexion à la base de données et exécute
        les instructions SQL dans le fichier pour créer les tables et initialiser la base de données.

        :return: None
        """
        # Ouverture du fichier 'db_init.sql' en mode lecture
        with open('db_init.sql', 'r') as f:
            # Lecture de toutes les lignes du fichier dans une liste
            sqlFile = f.readlines()

        # Établissement d'une connexion à la base de données en utilisant la méthode 'connection' de la classe 'DBConnection'
        conn = DBConnection().connection()
        # Création d'un objet 'cursor' à partir de la connexion
        cursor = conn.cursor()

        # Parcours de chaque ligne du fichier contenant les instructions SQL
        for row in sqlFile:
            # Exécution de chaque instruction SQL à l'aide de la méthode 'execute' du curseur
            cursor.execute(row)
            # Validation des modifications avec la méthode 'commit' de la connexion
            conn.commit()
