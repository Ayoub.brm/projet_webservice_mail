import datetime

class MailRequest():
    def __init__(self,sender,recipient,subject,body,send_at,statut = "NON TRAITE"):
        """
        Initialise un nouvel objet MailRequest avec les attributs spécifiés.
        
        Args:
        sender (str): l'adresse email de l'expéditeur.
        recipient (str): l'adresse email du destinataire.
        subject (str): le sujet du courrier électronique.
        body (str): le corps du courrier électronique.
        send_at (str ou datetime.datetime): la date et l'heure d'envoi du courrier électronique, au format "ddd mmm jj hh:mm:ss aaaa" ou un objet datetime.datetime.
        statut (str, optionnel): l'état de la demande de courrier électronique. Par défaut, "NON TRAITE".
        """
        self.__sender = sender
        self.__recipient = recipient
        self.__subject = subject
        self.__body = body
        if isinstance(send_at,str):
            # Si la date d'envoi est au format de chaîne de caractères, convertissez-la en objet datetime.datetime.
            send_at = datetime.datetime.strptime(send_at, "%a %b %d %H:%M:%S %Y")
        self.__send_at = send_at
        self.__statut = statut

    def get_sender(self):
        """
        Renvoie l'adresse email de l'expéditeur.
        """
        return self.__sender

    def get_recipient(self):
        """
        Renvoie l'adresse email du destinataire.
        """
        return self.__recipient
    
    def get_subject(self):
        """
        Renvoie le sujet du courrier électronique.
        """
        return self.__subject
    
    def get_body(self):
        """
        Renvoie le corps du courrier électronique.
        """
        return self.__body

    def get_send_at(self):
        """
        Renvoie la date et l'heure d'envoi du courrier électronique.
        """
        return self.__send_at
    
    def get_statut(self):
        """
        Renvoie l'état de la demande de courrier électronique.
        """
        return self.__statut

    def to_json(self):
        """
        Convertit l'objet MailRequest en une chaîne JSON représentant la demande de courrier électronique.
        
        Returns:
        request_json (dict): un dictionnaire contenant les informations de la demande de courrier électronique.
        """
        date_obj = self.__send_at
        # Convertit la date d'envoi en une chaîne de caractères au format "ddd mmm jj hh:mm:ss aaaa".
        self.send_at = date_obj.strftime("%a %b %d %H:%M:%S %Y")
        request_json = {
            "destinataire": self.__recipient, 
            "from": self.__sender, 
            "Subject": self.__subject,
            "contenu": self.__body,  
            "date-envoi": self.__send_at,
            "etat en cours": self.__statut 
        }
        return request_json