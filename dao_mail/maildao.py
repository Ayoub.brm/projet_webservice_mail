from dao_mail.dbconnection import DBConnection
from dao_mail.mail_request import MailRequest
import datetime 


class MailDAO():
    """
    Classe permettant de gérer l'insertion, la mise à jour et la récupération des mails dans une base de données.

    Methods
    -------
    insert(request)
        Insère une nouvelle demande de mail dans la base de données.
        Retourne l'id de la demande insérée.

    find_by_id(id)
        Récupère une demande de mail depuis la base de données à partir de son id.
        Retourne un objet MailRequest correspondant à la demande de mail trouvée, ou None si elle n'existe pas.

    past_query(date_now)
        Récupère les id des demandes de mails non traitées dont la date d'envoi est antérieure ou égale à la date donnée.
        Retourne une liste des id des demandes de mails non traitées trouvées.

    maj(list_id)
        Met à jour l'état des demandes de mails dont les id sont dans la liste donnée.
        Les demandes de mails sont marquées comme "TRAITE" dans la base de données.

    findAll()
        Récupère toutes les demandes de mails présentes dans la base de données.
        Retourne une liste d'objets MailRequest correspondant aux demandes de mails trouvées.
    """
    
    def insert(self, request):
        """
        Insère une nouvelle demande de mail dans la base de données.

        Parameters
        ----------
        request : MailRequest
            Objet MailRequest correspondant à la demande de mail à insérer dans la base de données.

        Returns
        -------
        int
            L'id de la demande insérée dans la base de données.
        """
        conn = DBConnection().connection()
        cursor = conn.cursor()
        cursor.execute("INSERT INTO table_demande(send_at, statut) VALUES (?, ?)", (request.get_send_at(), request.get_statut()))
        conn.commit()
        cursor.execute("SELECT MAX(id_demande) as id_last_demande FROM table_demande")
        row = cursor.fetchone()
        description = cursor.description
        nom_colonnes = [colonne[0] for colonne in description]
        valeurs = dict(zip(nom_colonnes, row))
        id_demande = int(valeurs['id_last_demande'])
        cursor.execute("INSERT INTO table_mail (id_demande, sender, recipient, about, body) VALUES (?, ?, ?, ?, ?)",
                       (id_demande, request.get_sender(), request.get_recipient(), request.get_subject(), request.get_body()))
        conn.commit()
        return id_demande
        
    def find_by_id(self, id):
        """
        Récupère une demande de mail depuis la base de données à partir de son id.

        Parameters
        ----------
        id : int
            L'id de la demande de mail à récupérer.

        Returns
        -------
        MailRequest
            Objet MailRequest correspondant à la demande de mail trouvée dans la base de données, ou None si elle n'existe pas.
        """
        conn = DBConnection().connection()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM table_mail WHERE id_demande = ?", (id,))
        mail = cursor.fetchone()
        if mail == None:
            return mail
        else:
            description = cursor.description
            nom_colonnes = [colonne[0] for colonne in description]
            mail = dict(zip(nom_colonnes, mail))
            cursor.execute("SELECT send_at, statut FROM table_demande WHERE id_demande = ?", (id,))
            res = cursor.fetchone()
            description = cursor.description
            nom_colonnes = [colonne[0] for colonne in description]
            res = dict(zip(nom_colonnes, res))
            date = res['send_at']
            date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            etat= res['statut']
            demande = MailRequest(mail["sender"],mail["recipient"],mail["about"],mail["body"],date,etat)

        return demande

    def past_query(self, date_now):
        """ Méthode permettant de récupérer les demandes non traitées avant une date donnée.

        Args:
            date_now (str): Date en format 'YYYY-MM-DD HH:MM:SS'.

        Returns:
            list: Liste des identifiants des demandes non traitées avant la date donnée.
        """
        list_id = []
        conn = DBConnection().connection()
        cursor = conn.cursor()
        cursor.execute("SELECT id_demande FROM table_demande WHERE send_at <= ? and statut = 'NON TRAITE'", (date_now,))
        row = cursor.fetchone()
        while row is not None:
            list_id.append(row[0])
            row = cursor.fetchone()
        return list_id

    def maj(self, list_id):
        """ Méthode permettant de mettre à jour le statut des demandes traitées dans la base de données.

        Args:
            list_id (list): Liste des identifiants des demandes traitées.

        Returns:
            None
        """
        conn = DBConnection().connection()
        cursor = conn.cursor()
        for id in list_id:
            cursor.execute("UPDATE table_demande SET statut = 'TRAITE' WHERE id_demande= ?", (id,))
            conn.commit()

    def findAll(self):
        """ Méthode permettant de récupérer toutes les demandes de la table_demande, avec leurs informations associées.

        Returns:
            list: Liste des instances de la classe MailRequest représentant les demandes de la table_demande.
        """
        conn = DBConnection().connection()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM (SELECT * FROM table_demande JOIN table_mail USING (id_demande) )")
        row = cursor.fetchone()
        requests = []
        description = cursor.description
        while row is not None:
            nom_colonnes = [colonne[0] for colonne in description]
            mail = dict(zip(nom_colonnes, row))
            date = mail["send_at"]
            date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            requests.append(MailRequest(mail["sender"], mail["recipient"], mail["about"], mail["body"], date, mail["statut"]))
            row = cursor.fetchone()
        return requests