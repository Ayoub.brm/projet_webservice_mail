# Webservice d'envoi de mail

## Description
Ce webservice permet d'envoyer des mails en utilisant un serveur SMTP local. Il offre les fonctionnalités suivantes :

- Créer une demande d'envoi par requête HTTP POST sur /demande/, renvoyant la demande avec un ID renseigné.
- Récupérer une demande par requête HTTP GET sur /demande/{demande-id}, renvoyant la demande et son état en cours.
- Lister toutes les demandes par requête HTTP GET sur /demandes/.

## Installation
Les dépendances requises pour exécuter cette application sont listées dans le fichier requirements.txt. Vous pouvez les installer en exécutant la commande suivante dans l'invite de commande:

```
python -m pip install -r requirements.txt
```
Installer aussi une extension pour communiquer avec l'API. Si vous utilisez Google Chrome installez Postman (https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop), dans le cas ou vous utilisez Firefox installez RESTClient (https://addons.mozilla.org/fr/firefox/addon/restclient/)

## Démarrage
Vous pouvez démarrer l'application en exécutant le script main.py. 
Il vous suffit de rentrer l'instruction suivante dans votre terminal.
(Pour le premier lancement vous devez d'abord impérativement gérer les dépendances pour cela aller à la rubrique [Installation].)

```
python main.py
```
Cela va lancer le serveur SMTP, créer la base de données, planifier les tâches d'envoi de mails et démarrer l'API Flask.
Le serveur sera alors lancé et vous pourrez accéder au Web Service via l'adresse http://localhost:80/.

## Configuration

Le fichier config.py contient les paramètres de configuration pour l'application. Vous pouvez y modifier les informations de connexion au serveur SMTP et à la base de données.

## Utilisation

Une fois l'application démarrée, vous pouvez envoyer une demande d'envoi de mail en envoyant une requête HTTP POST sur /demande/ avec les paramètres suivants :
- from : adresse email de l'expéditeur.
- destinataire : adresse email du destinataire.
- Subject : sujet du mail.
- contenu : contenu du mail.
- date-envoi : date d'envoi du mail

Voici un exemple de demande a envoyé ( attention il faut bien respecter le format de la date):
```
{
  "destinataire":"antoine.brunetti@insee.fr",
  "from":"bobby@eleve.ensai.fr"
  "Subject":"Rendu conception logicielle "
  "contenu":"Voici le lien de notre dépôt git sur gitlab https://gitlab.com/johndoe/conception , on a rendu en retard mais avec notre application, c'est cool non?"
  "date-envoi":"Fri Mar 24 23:41:00 2023"
}

```
L'API prend des Json en entrée du router POST.
Vous pouvez récupérer une demande d'envoi de mail en envoyant une requête HTTP GET sur /demande/{demande-id}, où {demande-id} est l'ID de la demande.

Vous pouvez lister toutes les demandes d'envoi de mails en envoyant une requête HTTP GET sur /demandes/.

## Auteur

Ce projet a été développé par Ayoub BERAMI.