from dao_mail.bdd_creation import Creation_bdd
from smtpserver.smtpservermanager import SMTPServerManager
import threading
from api_module.app import app
import asyncore
from service.scheduler import Scheduler
from config import API_HOST,  API_PORT

# Démarrage du serveur SMTP en tant que thread
server = SMTPServerManager().start()
smtp_thread = threading.Thread(target=asyncore.loop, name='smtp_server_thread')
smtp_thread.start()

# Création de la base de données
Creation_bdd().creation_bdd()

# Planification des tâches avec un intervalle de 20 secondes
planner = Scheduler(20)
thread = threading.Thread(target=planner.start,kwargs={'host': SMTPServerManager().get_host(), 'port': SMTPServerManager().get_port()})
thread.daemon = True
thread.start()

# Démarrage de l'API Flask sur le port 80 en tant que thread
api_thread = threading.Thread(target=app.run, kwargs={'host': API_HOST, 'debug': True, 'port': API_PORT, 'use_reloader': False})
api_thread.daemon = True
api_thread.start()