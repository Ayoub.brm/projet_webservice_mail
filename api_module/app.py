from flask import Flask, jsonify, request
from dao_mail.mail_request import MailRequest
from dao_mail.maildao import MailDAO

app = Flask(__name__)

@app.route('/demande/', methods=['POST'])
def creer_demande():
    """Crée une demande à partir des données JSON reçues, l'insère dans la base de données 
    et renvoie la demande créée au format JSON avec son identifiant et le code HTTP 201 (Créé).
    Si la création échoue, renvoie une erreur 500 (Erreur interne du serveur).

    Returns:
        Un objet JSON représentant la demande créée.
    """
    demande = request.get_json()
    mail = MailRequest(demande["from"],demande["destinataire"],demande["Subject"],demande["contenu"],demande["date-envoi"])
    id = MailDAO().insert(mail)
    demande['id_demande'] = id
    return  jsonify(demande),201

@app.route('/demande/<int:demande_id>', methods=['GET'])
def obtenir_demande(demande_id):
    """Récupère une demande de la base de données à partir de son identifiant, la renvoie au format JSON
    avec le code HTTP 200 (OK) si elle est trouvée. Sinon, renvoie une erreur 404 (Non trouvé) avec un message
    JSON indiquant que la demande n'a pas été trouvée.

    Args:
        demande_id (int): L'identifiant de la demande à récupérer.

    Returns:
        Un objet JSON représentant la demande trouvée ou un message JSON d'erreur si elle n'est pas trouvée.
    """
    demande = MailDAO().find_by_id(demande_id)
    if demande == None:
        return jsonify({'message': 'Demande non trouvée'}), 404
    else:
        demande = demande.to_json()
        return jsonify(demande), 200

@app.route('/demandes/', methods=['GET'])
def lister_demandes():
    """Récupère toutes les demandes de la base de données, les convertit en objets JSON et les renvoie 
    sous la forme d'un objet JSON contenant une liste de ces objets avec le code HTTP 200 (OK).

    Returns:
        Un objet JSON représentant toutes les demandes de la base de données.
    """
    mails = MailDAO().findAll()
    demandes = [mail.to_json() for mail in mails]
    return jsonify({'demandes': demandes}), 200