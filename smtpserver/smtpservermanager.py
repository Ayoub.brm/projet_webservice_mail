from smtpserver.smtp_server import CustomSMTPServer
from config import SERVER_SMTP_HOST , SERVER_SMTP_PORT

class SMTPServerManager():
    """
    Classe qui permet de gérer le serveur SMTP personnalisé.

    Attributs:
    ----------
    __host : str
        L'adresse IP ou le nom d'hôte du serveur SMTP personnalisé.
    __port : int
        Le numéro de port du serveur SMTP personnalisé.

    Méthodes:
    ---------
    get_host():
        Cette méthode permet d'obtenir l'adresse IP ou le nom d'hôte du serveur SMTP personnalisé.

    get_port():
        Cette méthode permet d'obtenir le numéro de port du serveur SMTP personnalisé.

    start():
        Cette méthode permet de démarrer le serveur SMTP personnalisé.

    stop():
        Cette méthode permet d'arrêter le serveur SMTP personnalisé.
    """

    def __init__(self):
        """
        Initialise une instance de la classe SMTPServerManager avec les valeurs par défaut.
        """
        self.__host = SERVER_SMTP_HOST 
        self.__port = SERVER_SMTP_PORT

    def get_host(self):
        """
        Permet d'obtenir l'adresse IP ou le nom d'hôte du serveur SMTP personnalisé.

        Retours:
        --------
        str
            L'adresse IP ou le nom d'hôte du serveur SMTP personnalisé.
        """
        return self.__host
    
    def get_port(self):
        """
        Permet d'obtenir le numéro de port du serveur SMTP personnalisé.

        Retours:
        --------
        int
            Le numéro de port du serveur SMTP personnalisé.
        """
        return self.__port
    
    def start(self):
        """
        Permet de démarrer le serveur SMTP personnalisé.

        Retours:
        --------
        CustomSMTPServer
            Une instance de la classe CustomSMTPServer représentant le serveur SMTP personnalisé.
        """
        return CustomSMTPServer((self.__host, self.__port), None)
    
    def stop(self):
        """
        Permet d'arrêter le serveur SMTP personnalisé.
        """
        CustomSMTPServer((self.__host, self.__port), None).close()