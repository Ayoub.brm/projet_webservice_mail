from smtpd import SMTPServer

class CustomSMTPServer(SMTPServer):
    """
    Classe pour personnaliser un serveur SMTP.

    Hérite de la classe SMTPServer de la bibliothèque smtpd.

    Attributes
    ----------
    Aucun attribut de classe.

    Methods
    -------
    process_message(peer, mailfrom, rcpttos, data, **kwargs)
        Méthode pour traiter un message reçu par le serveur SMTP.
        Imprime des informations sur le message reçu et appelle la méthode process_message de la classe mère.
    """

    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        """
        Méthode pour traiter un message reçu par le serveur SMTP.

        Parameters
        ----------
        peer : tuple
            Un tuple (adresse_IP, port) représentant le pair (IP, port) connecté à ce serveur SMTP.
        mailfrom : str
            L'adresse de l'expéditeur du message.
        rcpttos : list
            Une liste d'adresses de destinataires du message.
        data : bytes
            Les données du message, encodées en bytes.
        **kwargs : dict
            D'autres arguments optionnels.

        Returns
        -------
        Aucune valeur de retour.
        """
        print("Received message from:", peer)
        print("Message addressed from:", mailfrom)
        print("Message addressed to  :", rcpttos)
        print("Message length        :", len(data))
        print("Message               :", data)
        try:
            super().process_message(peer, mailfrom, rcpttos, data, **kwargs)
        except NotImplementedError:
            pass