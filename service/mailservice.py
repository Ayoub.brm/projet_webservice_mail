from dao_mail.maildao import MailDAO
from service.mailer import Mailer

class MailService():
    """
    Classe représentant le service d'envoi de mails.

    Methods:
    -------
    scheduledMail(self, time: datetime.datetime, host: str, port: int)
        Envoie les mails planifiés en utilisant la classe Mailer.
    
    Attributes:
    ----------
    Aucun attribut public n'est défini dans cette classe.
    """
    
    def scheduledMail(self, time , host: str, port: int):
        """
        Envoie les mails planifiés en utilisant la classe Mailer.

        Parameters:
        ----------
        time : datetime.datetime
            La date et l'heure actuelle utilisées pour récupérer les mails planifiés à envoyer.
        host : str
            L'hôte SMTP utilisé pour envoyer les mails.
        port : int
            Le port utilisé pour envoyer les mails.
        """
        id_to_send = MailDAO().past_query(time)
        liste_send = [MailDAO().find_by_id(id) for id in id_to_send]
        for mail in liste_send:
            Mailer().send(mail, host, port)
        MailDAO().maj(id_to_send)