import smtplib
from dao_mail.mail_request import MailRequest

class Mailer:
    """
    Classe représentant un objet permettant d'envoyer un mail.

    Methods:
    -------
    send(self, request: MailRequest, host: str, port: int)
        Envoie un mail à l'adresse du destinataire spécifié dans la demande.
        Args:
        -----
        request: MailRequest
            La demande d'envoi de mail.
        host : str
            L'hôte SMTP utilisé pour envoyer le mail.
        port : int
            Le port utilisé pour envoyer le mail.
    """
    def send(self, request, host, port):
        """
        Envoie un mail à l'adresse du destinataire spécifié dans la demande.

        Parameters:
        -----------
        request: MailRequest
            La demande d'envoi de mail.
        host : str
            L'hôte SMTP utilisé pour envoyer le mail.
        port : int
            Le port utilisé pour envoyer le mail.
        """
        conn = smtplib.SMTP(host, port)
        sender = request.get_sender()
        recipient = request.get_recipient()
        subject = request.get_subject()
        body = request.get_body()
        msg = subject + '\n' + body
        conn.sendmail(sender, [recipient], msg.encode('utf-8'))
        conn.quit()
