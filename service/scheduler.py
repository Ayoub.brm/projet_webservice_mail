import time
import datetime
from service.mailservice  import MailService

class Scheduler:
    """
    Classe représentant un planificateur d'envoi de mails.

    Attributes:
    ----------
    __intervall : int
        L'intervalle de temps en secondes entre chaque envoi de mail planifié.

    Methods:
    -------
    __init__(self, intervall: int)
        Constructeur de la classe Scheduler.

    start(self, host: str, port: int)
        Méthode pour démarrer le planificateur d'envoi de mails.
        Args:
        -----
        host: str
            L'hôte SMTP utilisé pour envoyer les mails.
        port: int
            Le port utilisé pour envoyer les mails.
    """
    def __init__(self,intervall):
        """
        Constructeur de la classe Scheduler.

        Parameters:
        -----------
        intervall : int
            L'intervalle de temps en secondes entre chaque envoi de mail planifié.
        """
        self.__intervall = intervall

    def start(self,host,port):
        """
        Méthode pour démarrer le planificateur d'envoi de mails.

        Args:
        -----
        host : str
            L'hôte SMTP utilisé pour envoyer les mails.
        port : int
            Le port utilisé pour envoyer les mails.
        """
        while True:
            MailService().scheduledMail(datetime.datetime.now(),host,port)
            time.sleep(self.__intervall)
